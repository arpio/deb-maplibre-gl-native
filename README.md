# MapLibre GL Native - Open-Source Mapbox GL Native

[![pipeline status](https://gitlab.com/arpio/deb-maplibre-gl-native/badges/main/pipeline.svg)](https://gitlab.com/arpio/deb-maplibre-gl-native/-/commits/main)

## Droidian Packaging

Packaging of [MapLibre GL Native](https://github.com/maplibre/maplibre-gl-native) for [Droidian](https://droidian.org)